package utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SelectorWrapper {
	private WebDriver driver;

	public SelectorWrapper(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement findElement(String type, String locator) {
		WebElement element = null;
		type = type.toLowerCase();

		if ("id".equals(type)) {
			element = driver.findElement(By.id(locator));
		} else if ("xpath".equals(type)) {
			element = driver.findElement(By.xpath(locator));
		} else if ("css".equals(type)) {
			element = driver.findElement(By.cssSelector(locator));
		}

		return element;
	}

}
