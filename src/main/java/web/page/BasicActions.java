package web.page;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.SelectorWrapper;

class BasicActions {
	private WebDriver driver;
	private String baseUrl;
	private SelectorWrapper sw;
	private JavascriptExecutor js;

	@BeforeClass
	void setUp() throws Exception {
		driver = new ChromeDriver();
		baseUrl = "https://www.phptravels.net/";
		sw = new SelectorWrapper(driver);
		js = (JavascriptExecutor) driver;
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(baseUrl);
	}

	@Test
//	void test() throws InterruptedException {
//		
//		WebElement element = gm.findElement("test", "test2");
//		driver.get(baseUrl);
//		String title = driver.getTitle();
//		String expectedTitle = "PHPTRAVELS | Travel Technology Partner";
//		Thread.sleep(3000);
//		assertEquals(expectedTitle, title);
//		System.out.println("Page is loaded");
//	}
	
	void test() throws InterruptedException {
		
		WebElement element = sw.findElement("xpath", "//a[@id='dropdownCurrency']/i");
		//element.click();
		Thread.sleep(2000);
		js.executeScript("arguments[0].click()", element);
	}
	

	@AfterClass
	void tearDown() throws Exception {
		Thread.sleep(2000);
		driver.quit();
	}

}
