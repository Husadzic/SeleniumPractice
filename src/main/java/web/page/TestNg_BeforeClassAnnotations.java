package web.page;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestNg_BeforeClassAnnotations {
    @BeforeMethod
    public void setUp() {
        System.out.println("Prije svake test metode");
    }

    @AfterMethod
    public void tearDown() {
        System.out.println("Poslije svake test metode");
    }

    @Test
    public void testMethod1() {
        System.out.println("Ide metoda jedan");
    }

    @Test
    public void testMethod2() {
        System.out.println("Ide metoda dva");
    }

}
