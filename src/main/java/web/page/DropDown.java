package web.page;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

class DropDown {

	private WebDriver driver;
	private String baseUrl;

	// https://learn.letskodeit.com/p/practice
	@BeforeClass
	void setUp() throws Exception {
		driver = new ChromeDriver();
		baseUrl = "https://learn.letskodeit.com/p/practice";
		driver.get(baseUrl);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test
	void testDropDown(){
		WebElement selectCars = driver.findElement(By.id("carselect"));
		Select selectCar = new Select(selectCars);
		List<WebElement> options = selectCar.getOptions();
		for (WebElement option : options){
			String optionName = option.getText();
			if("Benz".equals(optionName)){
				selectCar.selectByVisibleText(optionName);
				break;
			}
		}
	}

	@AfterClass
	void tearDown() throws Exception {
		driver.close();
	}

}
