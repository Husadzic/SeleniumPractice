package web.page;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeDemo {

	public static void main(String[] args) {
		WebDriver driver;
		//System.setProperty("webdriver.chrome.driver", "C:/Users/Jaso/eclipse-workspace/Selenium/chromedriver.exe");
		
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String baseURL = "https://www.google.ba";
		
		driver.get(baseURL);
	}

}
