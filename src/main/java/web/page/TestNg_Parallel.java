package web.page;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class TestNg_Parallel {
    private WebDriver driver;
    private String baseUrl;
    private ExtentReports report;
    private ExtentTest test;

    @BeforeClass
    @Parameters({"browser"})
    public void setUp(String browser) {
        baseUrl = "https://www.google.ba/";
        report = new ExtentReports("C:\\Users\\Jaso\\Desktop\\mojtest.html",false);
        test = report.startTest("Moj test");
        if (browser.equalsIgnoreCase("chrome")) {
            driver = new ChromeDriver();
        } else if (browser.equalsIgnoreCase("firefox")) {
            driver = new FirefoxDriver();
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(baseUrl);
        test.log(LogStatus.INFO, "URL je ucitan");

    }

    @Test
    public void typingTest() {
        Assert.assertTrue(true);
        test.log(LogStatus.PASS, "Test ti je prosao");
        //Reporter.log("This is parallel test", true););
    }

    @AfterClass
    public void tearDown() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
        report.endTest(test);
        report.flush();

    }
}
