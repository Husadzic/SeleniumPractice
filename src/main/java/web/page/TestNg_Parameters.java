package web.page;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestNg_Parameters {

    @BeforeClass
    @Parameters({"browser", "platform"})
    public void setUp( String browser, String platform) {
        System.out.println("Parameter -> setUp()");
        System.out.println("1. Parameter from xml file: " + browser);
        System.out.println("2. Parameter from xml file: " + platform);


    }

    @Test
    @Parameters({"response"})
    public void parametersTest(String response) {
        System.out.println("Parameter -> parametesTest()");
        System.out.println("Response from xml file: " + response);


    }

    @AfterClass
    public void tearDown() {
        System.out.println("Parameter -> tearDown()");


    }
}
