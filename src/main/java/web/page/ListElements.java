package web.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

class ListElements {
    private WebDriver driver;
    private String baseUrl;


    @BeforeClass
    void setUp() throws Exception {
        driver = new ChromeDriver();
        baseUrl = "https://learn.letskodeit.com/p/practice";
        driver.get(baseUrl);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    void checkRadioButtons() throws InterruptedException {
        boolean isChecked = false;
        List<WebElement> radioButtons = driver
                .findElements(By.xpath("//input[contains(@name,'cars') and contains(@type,'radio')]"));
        System.out.println(radioButtons);
        for (WebElement button : radioButtons) {
            isChecked = button.isSelected();
            if (!isChecked) {
                button.click();
                Thread.sleep(1000);
            }
        }
    }

    @AfterClass
    void tearDown() throws Exception {
        Thread.sleep(2000);
        driver.close();
    }

}
