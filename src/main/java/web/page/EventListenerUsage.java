package web.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class EventListenerUsage {

    public static void main(String[] args) {
        String baseUrl = "https://www.expedia.com/";
        WebDriver driver = new FirefoxDriver();

        EventFiringWebDriver eDriver = new EventFiringWebDriver(driver);
        HandleEventListener el = new HandleEventListener();
        eDriver.register(el);

        eDriver.get(baseUrl);
        eDriver.findElement(By.id("tab-flight-tab-hp")).click();
    }
}
