package web.page;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestNg_Grouping {
    @BeforeClass(alwaysRun = true)
    public void beforeClass(){

    }
    @Test(groups = { "cars","sedan" })
    public void testAudiA6(){
        System.out.println("Test Audi 6");
    }
    @Test
    public void testBMW(){
        System.out.println("Test BMW");
    }
    @Test(enabled = false)
    public void testHondaCBR(){
        System.out.println("Test Honda disabled");
    }
    @Test(timeOut = 300)
    public void testNinja() throws InterruptedException {
        System.out.println("Test Ninja");
        Thread.sleep(200);
    }

    @AfterClass
    public void afterClass(){
        System.out.println("After Class");
    }

}
