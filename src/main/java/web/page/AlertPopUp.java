package web.page;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

class AlertPopUp {
	private WebDriver driver;
	private String baseUrl;

	@BeforeClass
	void setUp() throws Exception {
		driver = new ChromeDriver();
		baseUrl = "https://learn.letskodeit.com/p/practice";
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(baseUrl);
		
	}

	@Test
	void test() throws InterruptedException {
		WebElement element = driver.findElement(By.id("alertbtn"));
		//element.click();
		element.sendKeys(Keys.ENTER);
		Alert alert = driver.switchTo().alert();
		Thread.sleep(2000);
		alert.accept();
		//Actions action = new Actions(driver);
		
	}
	
	@AfterClass
	void tearDown() throws Exception {
		Thread.sleep(2000);
		driver.quit();
	}

}
