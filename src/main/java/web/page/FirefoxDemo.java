package web.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxDemo {

	public static void main(String[] args) {
		WebDriver driver;
		//System.setProperty("webdriver.gecko.driver","C:/Users/Jaso/eclipse-workspace/Selenium/geckodriver.exe");
		driver = new FirefoxDriver();
		
		String baseURL = "https://www.google.ba";
		driver.get(baseURL);
		
		String currentURL = driver.getCurrentUrl();
		System.out.println(currentURL);
		driver.close();

	}

}
